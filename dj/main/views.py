from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.core.files.storage import FileSystemStorage
from dj.main.models import FileModel
from django.http import HttpResponse

def home(request):
    if request.method == "POST":
        uploaded_file = request.FILES['document']
        if uploaded_file.name.endswith('.txt'):
            f = FileModel(name=uploaded_file.name, user=request.user)
            fs = FileSystemStorage()
            fs.save(str(f.id), uploaded_file)

            if fs.exists(str(f.id)):
                f.save()
    return render(request, "main/home.html", {})

def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, "main/success.html", {})
    else:
        form = UserCreationForm()
    return render(request, "main/signup.html", {"form":form})

def download(request, id):
    f = FileModel.objects.get(id=id)
    fs = FileSystemStorage()
    download_file = fs.open(str(id))
    response = HttpResponse(download_file)
    response['content_type'] = 'file/txt'
    response['Content-Disposition'] = 'attachment;filename={}'.format(f.name)
    return response

def user_data(request):
    if request.method == "POST":
        email = request.POST.get('email_input')
        first_name = request.POST.get('first_name_input')
        last_name = request.POST.get('last_name_input')
        if not (email is None or first_name is None or last_name is None):

            user = request.user
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.save()

        return render(request, "main/home.html", {})
    return render(request, "main/user_data.html", {})
