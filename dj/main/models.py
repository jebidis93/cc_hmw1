import uuid
from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage

class FileModel(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='files', null=True)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)

    @property
    def counts_of_words(self):
        counts = {}
        alphabet = ' abcdefghijklmnopqrstuvwxyz'
        fs = FileSystemStorage()
        if fs.exists(str(self.id)):
            f = fs.open(str(self.id), mode='r')
            content = f.read().lower()
            for char in content:
                if char not in alphabet:
                    content = content.replace(char, '')
            content = ' '.join(content.split())
            content_list = content.split(' ')
            for word in set(content_list):
                counts[word] = 0

            for word in content_list:
                counts[word] += 1

        return counts
