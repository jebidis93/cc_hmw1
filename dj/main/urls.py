from django.urls import path
from dj.main.views import home, signup, download, user_data
app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('signup', signup, name='signup'),
    path('download/<id>', download, name='download'),
    path('data', user_data, name="data")
]
